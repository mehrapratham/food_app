import { combineReducers } from 'redux';
import { routerMiddleware, routerReducer } from 'react-router-redux';
import Login from './Login';
/*combine reducers*/
export default combineReducers({
 router: routerReducer,
 Login: Login
});