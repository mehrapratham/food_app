import initialState from '../initialState'
import { CURRENT_ORDER, UPDATE_INPROGRESS, UPDATE_ORDER_HISTORY } from '../../actions/types'
export default (state = initialState.data, action) => {
 switch (action.type) {
   case CURRENT_ORDER: 
   	return {...state, current_order: action.payload}
   case UPDATE_INPROGRESS: 
   	return {...state, inprogress_order: action.payload}
   case UPDATE_ORDER_HISTORY: 
   	return {...state, all_orders: action.payload}

   	
   default:
    return state
 }
}