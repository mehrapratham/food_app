export default {
	data: {
		loginData: {
			username: 'Test',
			password: '123456'
		},
		new_order: {
			id: 1,
			order_no: 'OR1234',
			first_name: 'Surinder',
			last_name: 'Singh',
			phone: 9855857725,
			status: 'In Progress',
			date: '10 Jan 8:45 PM',
			total: 420,
			order_detail: [
				{
					meal_name: 'Combination Kebab',
					rate: 120,
					extra_detail: 'This is dummy text for this meal as detail',
					label: 'NON-VEG'
				},
				{
					meal_name: 'Pizza',
					rate: 100,
					extra_detail: 'This is dummy text for this meal as detail',
					label: 'VEG'
				},
				{
					meal_name: 'Pizza Minia',
					rate: 80,
					extra_detail: 'This is dummy text for this meal as detail',
					label: 'VEG'
				},
				{
					meal_name: 'Pizza Farmhouse',
					rate: 120,
					extra_detail: 'This is dummy text for this meal as detail',
					label: 'VEG'
				}
			],
			address: {
				address1: 'F301, Phase 8b, Industrail Area',
				address2: 'Dist Mohali',
				city: 'Mohali',
				Pin: '160055'
			},
			deliver_by: {
				licence_no: '87656789',
				name: 'Ajay Singh',
				phone: '987678987',
				avatar: 'img'
			}
		},
		inprogress_order: [
			
		],
		current_order: {},
		all_orders: [
			{
				id: 1,
				order_no: 'OR987',
				first_name: 'Rupinder',
				last_name: 'Singh',
				phone: 9855857725,
				status: 'completed',
				date: '10 Jan 8:45 PM',
				total: 360,
				order_detail: [
					{
						meal_name: 'Combination Kebab',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'NON-VEG'
					},
					{
						meal_name: 'Pizza',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					},
					{
						meal_name: 'Pizza Minia',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					}
				],
				address: {
					address1: 'VPO Talwandi Jalle khan',
					address2: 'Dist Ferozepur',
					city: 'Zira',
					Pin: '152028'
				},
				deliver_by: {
					licence_no: '87656789',
					name: 'Ajay Singh',
					phone: '987678987',
					avatar: 'img'
				}
			},
			{
				id: 2,
				order_no: 'OR987',
				first_name: 'Pratham',
				last_name: 'Mehra',
				phone: 9855857725,
				status: 'completed',
				date: '10 Jan 8:45 PM',
				total: 240,
				order_detail: [
					{
						meal_name: 'Combination Kebab',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'NON-VEG'
					},
					{
						meal_name: 'Pizza',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					}
				],
				address: {
					address1: 'VPO Talwandi Jalle khan',
					address2: 'Dist Ferozepur',
					city: 'Zira',
					Pin: '152028'
				},
				deliver_by: {
					licence_no: '87656789',
					name: 'Ajay Singh',
					phone: '987678987',
					avatar: 'img'
				}
			},
			{
				id: 3,
				order_no: 'OR987',
				first_name: 'Sapna',
				last_name: 'Gupta',
				phone: 9855857725,
				status: 'cancelled',
				date: '10 Jan 8:45 PM',
				total: 240,
				order_detail: [
					{
						meal_name: 'Pizza',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					},
					{
						meal_name: 'Pizza Minia',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					}
				],
				address: {
					address1: 'VPO Talwandi Jalle khan',
					address2: 'Dist Ferozepur',
					city: 'Zira',
					Pin: '152028'
				},
				deliver_by: {
					licence_no: '87656789',
					name: 'Ajay Singh',
					phone: '987678987',
					avatar: 'img'
				}
			},
			{
				id: 4,
				order_no: 'OR987',
				first_name: 'Mani',
				last_name: 'Sharma',
				phone: 9855857725,
				status: 'completed',
				date: '10 Jan 8:45 PM',
				total: 240,
				order_detail: [
					{
						meal_name: 'Combination Kebab',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'NON-VEG'
					},
					{
						meal_name: 'Pizza',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					},
					{
						meal_name: 'Pizza Minia',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					}
				],
				address: {
					address1: 'VPO Talwandi Jalle khan',
					address2: 'Dist Ferozepur',
					city: 'Zira',
					Pin: '152028'
				},
				deliver_by: {
					licence_no: '87656789',
					name: 'Ajay Singh',
					phone: '987678987',
					avatar: 'img'
				}
			},
			{
				id: 5,
				order_no: 'OR987',
				first_name: 'Swati',
				last_name: 'Sharma',
				phone: 9855857725,
				status: 'completed',
				date: '10 Jan 8:45 PM',
				total: 360,
				order_detail: [
					{
						meal_name: 'Combination Kebab',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'NON-VEG'
					},
					{
						meal_name: 'Pizza',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					},
					{
						meal_name: 'Pizza Minia',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					}
				],
				address: {
					address1: 'VPO Talwandi Jalle khan',
					address2: 'Dist Ferozepur',
					city: 'Zira',
					Pin: '152028'
				},
				deliver_by: {
					licence_no: '87656789',
					name: 'Ajay Singh',
					phone: '987678987',
					avatar: 'img'
				}
			},
			{
				id: 6,
				order_no: 'OR987',
				first_name: 'Test',
				last_name: 'Singh',
				phone: 9855857725,
				status: 'completed',
				date: '10 Jan 8:45 PM',
				total: 360,
				order_detail: [
					{
						meal_name: 'Combination Kebab',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'NON-VEG'
					},
					{
						meal_name: 'Pizza',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					},
					{
						meal_name: 'Pizza Minia',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					}
				],
				address: {
					address1: 'VPO Talwandi Jalle khan',
					address2: 'Dist Ferozepur',
					city: 'Zira',
					Pin: '152028'
				},
				deliver_by: {
					licence_no: '87656789',
					name: 'Ajay Singh',
					phone: '987678987',
					avatar: 'img'
				}
			},
			{
				id: 7,
				order_no: 'OR987',
				first_name: 'Hello',
				last_name: 'Singh',
				phone: 9855857725,
				status: 'completed',
				date: '10 Jan 8:45 PM',
				total: 360,
				order_detail: [
					{
						meal_name: 'Combination Kebab',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'NON-VEG'
					},
					{
						meal_name: 'Pizza',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					},
					{
						meal_name: 'Pizza Minia',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					}
				],
				address: {
					address1: 'VPO Talwandi Jalle khan',
					address2: 'Dist Ferozepur',
					city: 'Zira',
					Pin: '152028'
				},
				deliver_by: {
					licence_no: '87656789',
					name: 'Ajay Singh',
					phone: '987678987',
					avatar: 'img'
				}
			},
			{
				id: 8,
				order_no: 'OR987',
				first_name: 'Rahul',
				last_name: 'Sachdeva',
				phone: 9855857725,
				status: 'cancelled',
				date: '10 Jan 8:45 PM',
				total: 360,
				order_detail: [
					{
						meal_name: 'Combination Kebab',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'NON-VEG'
					},
					{
						meal_name: 'Pizza',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					},
					{
						meal_name: 'Pizza Minia',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					}
				],
				address: {
					address1: 'VPO Talwandi Jalle khan',
					address2: 'Dist Ferozepur',
					city: 'Zira',
					Pin: '152028'
				},
				deliver_by: {
					licence_no: '87656789',
					name: 'Ajay Singh',
					phone: '987678987',
					avatar: 'img'
				}
			},
			{
				id: 9,
				order_no: 'OR987',
				first_name: 'Raspinder',
				last_name: 'Singh',
				phone: 9855857725,
				status: 'completed',
				date: '10 Jan 8:45 PM',
				total: 360,
				order_detail: [
					{
						meal_name: 'Combination Kebab',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'NON-VEG'
					},
					{
						meal_name: 'Pizza',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					},
					{
						meal_name: 'Pizza Minia',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					}
				],
				address: {
					address1: 'VPO Talwandi Jalle khan',
					address2: 'Dist Ferozepur',
					city: 'Zira',
					Pin: '152028'
				},
				deliver_by: {
					licence_no: '87656789',
					name: 'Ajay Singh',
					phone: '987678987',
					avatar: 'img'
				}
			},
			{
				id: 10,
				order_no: 'OR987',
				first_name: 'Mandeep',
				last_name: 'Kaur',
				phone: 9855857725,
				status: 'completed',
				date: '10 Jan 8:45 PM',
				total: 360,
				order_detail: [
					{
						meal_name: 'Combination Kebab',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'NON-VEG'
					},
					{
						meal_name: 'Pizza',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					},
					{
						meal_name: 'Pizza Minia',
						rate: 120,
						extra_detail: 'This is dummy text for this meal as detail',
						label: 'VEG'
					}
				],
				address: {
					address1: 'VPO Talwandi Jalle khan',
					address2: 'Dist Ferozepur',
					city: 'Zira',
					Pin: '152028'
				},
				deliver_by: {
					licence_no: '87656789',
					name: 'Ajay Singh',
					phone: '987678987',
					avatar: 'img'
				}
			}
		]
	}
	

};