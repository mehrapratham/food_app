import React from 'react'
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, View,Drawer, Card, CardItem, Thumbnail } from 'native-base';
import {TextInput,Image,Dimensions,ScrollView,TouchableOpacity} from 'react-native'
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import MainHeader from '../../components/common/MainHeader'
import Dialog, { SlideAnimation, DialogContent } from 'react-native-popup-dialog';
import {updateInprogress, updateOrderHistory, selectOrder} from '../../actions/MarketPlace'
 class InprogressCard extends React.Component{
	constructor(props){
		super(props);
		this.state = {
		}
	}


	deliverOrder(index){
		let {new_order, inprogress_order} = this.props.Login
		inprogress_order = JSON.parse(JSON.stringify(inprogress_order))
		inprogress_order[index].status = 'Deliver'
		this.props.dispatch(updateInprogress(inprogress_order))
	}

	completeOrder(index){
		let {new_order, inprogress_order, all_orders} = this.props.Login
		inprogress_order = JSON.parse(JSON.stringify(inprogress_order))

		all_orders = JSON.parse(JSON.stringify(all_orders))
		inprogress_order[index].status = 'completed'
		all_orders.unshift(inprogress_order[index])
		this.props.dispatch(updateOrderHistory(all_orders))
		inprogress_order.splice(index, 1)
		this.props.dispatch(updateInprogress(inprogress_order))
		Actions.OrderHistory();
	}

	
	render(){
		let {data} = this.props;
		return(
			<View style={{backgroundColor: '#000', borderRadius: 5, padding: 15, marginBottom: 15}}>
				<View style={{marginBottom: 10}}>
					<Text style={{color: '#fff', fontSize: 18}}>{data.first_name +' '+data.last_name}(#{data.order_no})</Text>
					<Text style={{color: '#fff', position: 'absolute', top: 0, right: 0, fontSize: 18}}>₹ {data.total}</Text>
				</View>
				<View style={{marginBottom: 20}}>
					<Text style={{color: '#fff'}}>Address:</Text>
					<Text style={{color: '#fff'}}>{data.address.address1}</Text>
					<Text style={{color: '#fff'}}>{data.address.address2}</Text>
					<Text style={{color: '#fff'}}>{data.address.city}, {data.address.Pin}</Text>
				</View>
				<View style={{marginBottom:20}}>
					<Button small light onPress={this.props.openModal.bind(this, data)}>
			            <Text>View Order</Text>
			          </Button>
				</View>
				<View style={{marginBottom: 20}}>
					<Text style={{color: '#fff', marginBottom:10}}>Courier</Text>

					<View style={{paddingLeft: 50, position:'relative'}}>
						<View style={{width: 40, height: 40, borderRadius: 20, backgroundColor: '#fff', position: 'absolute', left: 0}}>
							<Image source={require('../../img/user.png')} style={{width: '100%',height: '100%'}}/>
						</View>
						<Text style={{color: '#fff'}}>{data.deliver_by.name}</Text>
						<Text style={{color: '#fff'}}>{data.deliver_by.phone}</Text>
					</View>


				</View>
				<View style={{marginBottom:20}}>
					<Text style={{color: '#fff'}}>Status: <Text style={{color: 'green'}}>{data.status}</Text></Text>
				</View>
				<View>
					{data.status == 'Deliver' ? <Button block warning onPress={this.completeOrder.bind(this, this.props.index)}>
														            <Text>Complete</Text>
														          </Button> :
														        <Button block success onPress={this.deliverOrder.bind(this, this.props.index)}>
														            <Text>Deliver</Text>
														          </Button>
														        }
				</View>
			</View>
		)
	}
}
export default connect(state => ({}, mapDispatch))(InprogressCard);


const mapDispatch = (dispatch) => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
}