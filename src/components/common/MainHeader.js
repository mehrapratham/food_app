import React from 'react'
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, View,Drawer } from 'native-base';
import {TextInput,Image,Dimensions,ScrollView,TouchableOpacity} from 'react-native'
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import Dialog, { SlideAnimation, DialogContent } from 'react-native-popup-dialog';
 class MainHeader extends React.Component{
 	is_mounted = false;
	constructor(props){
		super(props);
		this.state = {
			isOpened: false,
			modalVisible: false,
			selected: null
		}
	}
	componentDidMount(){
		this.is_mounted = true;
	}
	closeDrawer(){
		if(this.is_mounted){
    	this.drawer && this.drawer._root.close()
    	this.setState({ isOpened: false })
    }
  }
    openDrawer(){
    	if(this.is_mounted){
        	this.drawer && this.drawer._root.open()
	        this.setState({ isOpened: true })
	    }
    }
    goToOrderHistory = () => {
    	this.closeDrawer()
    	Actions.OrderHistory();
    }

    goToInProgress = () =>{
    	this.closeDrawer()
    	Actions.InProgress();
    }

    goToNewOrder = () =>{
    	this.closeDrawer()
    	Actions.StartScreen();
    }
    goToPauseOrder = () => {
    	this.closeDrawer()
    	this.setState({modalVisible: true})
    }
    onPressPause = (key) => {
    	if(key){
    		this.setState({ selected: key})
    	}
    }
    componentWillUnMount(){
    	this.is_mounted = false
    }
	
	render(){
		var {height, width} = Dimensions.get('window');
		let {isOpened} = this.state;
		const slideAnimation = new SlideAnimation({
            slideFrom: 'bottom',
        });
		return(
			<Container>
		        <Content contentContainerStyle={{flex: 1}}>
    				<ScrollView>
    					<View>
    						<View style={{height: 60,backgroundColor: '#3e9920',paddingTop: 20,paddingLeft: 20,flexDirection: 'row'}}>
    							{!isOpened ? 
	    							<TouchableOpacity onPress={this.openDrawer.bind(this)}>
	    								<Icon name="menu" style={{fontSize: 26,color: '#fff'}}/>
	    							</TouchableOpacity>:
	    							<TouchableOpacity onPress={this.closeDrawer.bind(this)}>
	    								<Icon name="close" style={{fontSize: 32,color: '#fff'}}/>
	    							</TouchableOpacity>}
    						</View>
					        <View style={{width,height: height-60}}>
								<Drawer
						          ref={(ref) => { this.drawer = ref; }}
						          content={<View style={{backgroundColor: '#3e9920',height: '100%'}}>
						                    <ScrollView style={{padding: 10}}>
						                    	<TouchableOpacity style={{padding: 10}} onPress={this.goToNewOrder}>
						                    		<Text style={{color: '#fff',fontSize: 18}}>New Order</Text>
						                    	</TouchableOpacity>
						                    	<TouchableOpacity style={{padding: 10}} onPress={this.goToInProgress}>
						                    		<Text style={{color: '#fff',fontSize: 18}}>In Progress</Text>
						                    	</TouchableOpacity>
						                    	<TouchableOpacity style={{padding: 10}} onPress={this.goToOrderHistory}>
						                    		<Text style={{color: '#fff',fontSize: 18}}>Order History</Text>
						                    	</TouchableOpacity>
						                    	<TouchableOpacity style={{padding: 10}} onPress={this.goToPauseOrder}>
						                    		<Text style={{color: '#fff',fontSize: 18}}>Pause Order</Text>
						                    	</TouchableOpacity>
						                    </ScrollView>
						                  </View>}
						          tapToClose={true}
	          					  openDrawerOffset={0.3}
						          onClose={() => this.closeDrawer()} 
						          >

							        {this.props.child}
						        </Drawer>
					        </View>
    					</View>
    				</ScrollView>
    				<Dialog
		                    visible={this.state.modalVisible}
		                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
		                    dialogAnimation={slideAnimation}
		                    dialogStyle={{borderRadius: 4,width: '100%'}}
		                    containerStyle={{justifyContent: 'center',padding: 10}}
		                    onTouchOutside={() => {
		                        this.setState({ modalVisible: false });
		                     }}
		                >
		                    <DialogContent>
		                        <View style={{position: 'relative'}}>
		                            <TouchableOpacity
		                                style={{position: 'absolute',right: 0,top:5,paddingLeft: 10,zIndex: 100}}
		                                onPress={() => {
		                                    this.setState({ modalVisible: false });
		                                }}>
		                                <Text style={{ fontSize: 16 }}>X</Text>
		                            </TouchableOpacity>
		                            <View style={{paddingTop: 20,paddingBottom: 0}}>
		                            	<Text style={{fontSize: 20,marginBottom: 10}}>Pause New Orders</Text>
		                            	<Text style={{marginBottom: 10}}>How long would you like to pause new orders?</Text>
		                            	<View style={{flexWrap: 'wrap',flexDirection: 'row'}}>
		                            		<Button small style={{borderRadius: 0,borderColor: 'grey',borderWidth: 0.5,backgroundColor: this.state.selected == '10min' ?'#3e9920' : '#fff',marginRight: 10,marginBottom: 10}} onPress={this.onPressPause.bind(this,'10min')}>
									            <Text style={{color: this.state.selected == '10min' ?'#fff' : '#000'}}>10 min</Text>
									        </Button>
									        <Button small style={{borderRadius: 0,borderColor: 'grey',borderWidth: 0.5,backgroundColor: this.state.selected == '15min' ?'#3e9920' : '#fff',marginRight: 10,marginBottom: 10}} onPress={this.onPressPause.bind(this,'15min')}>
									            <Text style={{color: this.state.selected == '15min' ?'#fff' : '#000'}}>15 min</Text>
									        </Button>
									        <Button small style={{borderRadius: 0,borderColor: 'grey',borderWidth: 0.5,backgroundColor: this.state.selected == '20min' ?'#3e9920' : '#fff',marginRight: 10,marginBottom: 10}} onPress={this.onPressPause.bind(this, '20min')}>
									            <Text style={{color: this.state.selected == '20min' ?'#fff' : '#000'}}>20 min</Text>
									        </Button>
									        <Button small style={{borderRadius: 0,borderColor: 'grey',borderWidth: 0.5,backgroundColor: this.state.selected == '25min' ?'#3e9920' : '#fff',marginRight: 10,marginBottom: 10}} onPress={this.onPressPause.bind(this, '25min')}>
									            <Text style={{color: this.state.selected == '25min' ?'#fff' : '#000'}}>25 min</Text>
									        </Button>
									        <Button small style={{borderRadius: 0,borderColor: 'grey',borderWidth: 0.5,backgroundColor: this.state.selected == '30min' ?'#3e9920' : '#fff',marginRight: 10,marginBottom: 10}} onPress={this.onPressPause.bind(this, '30min')}>
									            <Text style={{color: this.state.selected == '30min' ?'#fff' : '#000'}}>30 min</Text>
									        </Button>
									        <Button small style={{borderRadius: 0,borderColor: 'grey',borderWidth: 0.5,backgroundColor: this.state.selected == 'day' ?'#3e9920' : '#fff',marginRight: 10,marginBottom: 10}} onPress={this.onPressPause.bind(this, 'day')}>
									            <Text style={{color: this.state.selected == 'day' ?'#fff' : '#000'}}>Rest of the day</Text>
									        </Button>
		                            	</View>
		                            	<View style={{alignSelf: 'flex-end',flexDirection: 'row'}}>
		                            		<Button small style={{borderRadius: 0,backgroundColor: '#fff'}} onPress={() => this.setState({ modalVisible: false })}>
									            <Text style={{color: '#000'}}>CANCEL</Text>
									        </Button>
									        <Button small style={{borderRadius: 0,borderColor: 'grey',borderWidth: 0.5,backgroundColor: !this.state.selected ? 'grey': '#3e9920'}} disabled={!this.state.selected} onPress={() => this.setState({modalVisible: false})}>
									            <Text style={{color: '#fff'}}>NEXT</Text>
									        </Button>
		                            	</View>

		                            </View>
		                        </View>
		                        
		                    </DialogContent>
		                </Dialog>
		        </Content>
		    </Container>
		)
	}
}
export default connect(state => ({}, mapDispatch))(MainHeader);


const mapDispatch = (dispatch) => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
}