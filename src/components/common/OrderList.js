import React from 'react'
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, View,Drawer } from 'native-base';
import {TextInput,Image,Dimensions,ScrollView,TouchableOpacity} from 'react-native'
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import MainHeader from '../../components/common/MainHeader'
import Dialog, { SlideAnimation, DialogContent } from 'react-native-popup-dialog';
import {selectOrder} from '../../actions/MarketPlace'
 class OrderList extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			
		}
	}

	
	render(){
		var {height, width} = Dimensions.get('window');
		let {data} = this.props
		return(
			<TouchableOpacity style={{borderBottomWidth: 1,borderColor: '#D6D4D4',padding: 10}} activeOpacity={0.6} onPress={this.props.openModal.bind(this, data)}>
				<View style={{marginBottom: 5}}>
					<Text style={{fontSize: 16,fontWeight: '600'}}>{data.first_name + ' ' +data.last_name}</Text>
					<View style={{position: 'absolute',right: 0,top: 2}}>
						<Text style={{color: '#939090',fontSize: 14}}>₹ {data.total || 0}</Text>
					</View>
				</View>
				<View style={{flexDirection: 'row'}}>
					<View style={{padding: 2,paddingLeft: 10,paddingRight: 10, paddingBottom: 4,backgroundColor: data.status == 'cancelled'? 'red' : '#3e9920',borderRadius: 10}}>
						<Text style={{fontSize: 12,color: '#fff'}}>{data.status}</Text>
					</View>
					<View style={{position: 'absolute',right: 0}}>
						<Text style={{color: '#939090',fontSize: 14}}>{data.date}</Text>
					</View>
				</View>
			</TouchableOpacity>
		)
	}
}
export default connect(state => ({}, mapDispatch))(OrderList);


const mapDispatch = (dispatch) => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
}