import React from 'react'
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, View,Drawer, Card, CardItem, Thumbnail } from 'native-base';
import {TextInput,Image,Dimensions,ScrollView,TouchableOpacity} from 'react-native'
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import MainHeader from '../../components/common/MainHeader'
import InprogressCard from '../../components/common/InprogressCard'
import Dialog, { SlideAnimation, DialogContent } from 'react-native-popup-dialog';
import {updateInprogress, updateOrderHistory, selectOrder} from '../../actions/MarketPlace'
 class Inprogress extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			modalVisible: false,
		}
	}

	openModal(item){
		this.props.dispatch(selectOrder(item))
		this.setState({modalVisible: true})
	}

	deliverOrder(index){
		let {new_order, inprogress_order} = this.props.Login
		inprogress_order = JSON.parse(JSON.stringify(inprogress_order))
		inprogress_order[index].status = 'Deliver'
		this.props.dispatch(updateInprogress(inprogress_order))
	}

	completeOrder(index){
		let {new_order, inprogress_order, all_orders} = this.props.Login
		inprogress_order = JSON.parse(JSON.stringify(inprogress_order))

		all_orders = JSON.parse(JSON.stringify(all_orders))
		inprogress_order[index].status = 'completed'
		all_orders.unshift(inprogress_order[index])
		this.props.dispatch(updateOrderHistory(all_orders))
		inprogress_order.splice(index, 1)
		this.props.dispatch(updateInprogress(inprogress_order))
		Actions.OrderHistory();
	}

    renderChild(){
    	const slideAnimation = new SlideAnimation({
            slideFrom: 'bottom',
        });
    	let {current_order} = this.props.Login;
    	return 	<ScrollView style={{padding: 10, paddingBottom: 30}}>
    						{(this.props.Login.inprogress_order.length == 0) ? <View><Text style={{alignSelf: 'center', fontSize: 18, marginTop: 20}}>No Order in progress</Text></View> : <View></View>}
    						
    						{/*this.props.Login.inprogress_order.length && <View>{this.props.Login.inprogress_order.map((item, index) =>{
    							return <View><InprogressCard data={item} key={index} index={index} openModal={this.openModal.bind(this)} /></View>
    						})}</View>*/}

    						{this.props.Login.inprogress_order.length ? <View>
    							{this.props.Login.inprogress_order.map((item, index) =>{
		    							return <View><InprogressCard data={item} key={index} index={index} openModal={this.openModal.bind(this)} /></View>
		    						})}
    						</View> : <View></View>}

    						<Dialog
		                    visible={this.state.modalVisible}
		                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
		                    dialogAnimation={slideAnimation}
		                    dialogStyle={{borderRadius: 4,width: '100%'}}
		                    containerStyle={{justifyContent: 'center',padding: 10}}
		                    onTouchOutside={() => {
		                        this.setState({ modalVisible: false });
		                      }}
		                >
		                    <DialogContent>
		                        <View style={{position: 'relative'}}>
		                            <TouchableOpacity
		                                style={{position: 'absolute',right: 0,top:15,paddingLeft: 10,zIndex: 100}}
		                                onPress={() => {
		                                    this.setState({ modalVisible: false });
		                                }}>
		                                <Text style={{ fontSize: 16 }}>X</Text>
		                            </TouchableOpacity>
		                            <View style={{paddingBottom: 10,paddingTop: 40}}>
			                            {current_order.order_detail && current_order.order_detail.length && current_order.order_detail.map((order_item,key) => {
			                            	return(
			                            		<View style={{borderBottomWidth: 1,borderColor: '#D6D4D4',padding: 10,paddingLeft: 20}} key={key}>
					                            	<View style={{position: 'absolute',left: 0,top: 10}}>
					                            		<Text>{key+1}.</Text>
					                            	</View>
													    						<View style={{marginBottom: 2}}>
													    							<Text style={{fontSize: 14,fontWeight: '600'}}>{order_item.meal_name}</Text>
													    							<View style={{position: 'absolute',right: 0,top: 2}}>
													    								<Text style={{color: '#939090',fontSize: 14}}>{order_item.rate}</Text>
													    							</View>
													    						</View>
													    						<View style={{flexDirection: 'row'}}>
													    							<Text style={{color: '#939090',fontSize: 12}}>{order_item.label}</Text>
													    						</View>
													    					</View>
			                            	)
			                            })}
			                        </View>
		                        </View>
		                        
		                    </DialogContent>
		                </Dialog>


    					</ScrollView>
    }
	
	render(){
		return(
			<MainHeader child={this.renderChild()} />
		)
	}
}
export default connect(state => ({}, mapDispatch))(Inprogress);


const mapDispatch = (dispatch) => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
}