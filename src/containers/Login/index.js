import React from 'react'
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, View } from 'native-base';
import {TextInput,Image,Dimensions,ScrollView} from 'react-native'
import { IsValidForm } from '../../components/common/validation'
import Toast from 'react-native-easy-toast'
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import { LinearGradient } from 'expo';
 class Login extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			loginData: {
				username: '',
				password: ''
			},
			errors: {}
		}
	}
	onChangeText(key,event){
		let {loginData} = this.state;
		loginData[key] = event
		this.setState({loginData})
	}

	onSubmit(){
		let {loginData} = this.props.Login
		let fields = ['username', 'password']
		let formValidation = IsValidForm(fields, this.state.loginData)
		//this.setState({ errors: formValidation.errors })
		if (formValidation.validate) {
			if(loginData){
				if(loginData.username == this.state.loginData.username && loginData.password == this.state.loginData.password){
					Actions.StartScreen();
				}else{
					return this.refs.toast.show('Wrong username or password')
				}
			}
		}else{
			
		}
		
	}
	render(){
		let {username,password} = this.state.loginData;
		var {height, width} = Dimensions.get('window')
		return(
			<Container>
		        <Content contentContainerStyle={{flex: 1}}>
    				<LinearGradient
				        colors={['#3e9920', '#0f2712']}
				        start={[0.1, 0.1]} end={[1.3, 1.2]}
				        style={{flex: 1}}
				      >
			        <ScrollView style={{flex: 1,padding: 10}}>

				        <View style={{flex: 1,justifyContent: 'center',alignItems: 'center'}}>
				        	<View style={{height: 150,width: 270}}>
				        		<Image source={require('../../img/food.png')} style={{width: '100%',height: '100%'}}/>
				        	</View>
					    </View>
					    <View style={{flex: 2,justifyContent: 'flex-start',alignItems: 'center',paddingTop: 50}}>
					    	<View style={{width: '100%'}}>
						    	<TextInput
							        style={{height: 40, borderColor: 'gray', borderBottomWidth: 1,width: '100%',textAlign: 'center',borderColor: '#fff',marginBottom: 20,color: '#fff',fontSize: 16}}
							        onChangeText={this.onChangeText.bind(this, 'username')}
							        value={username}
							        placeholder={'Username'}
							        placeholderTextColor="#fff"
							    />
							    {this.state.errors.username && <Text style={{position: 'absolute',bottom: -5,color: 'red',fontSize: 14,alignSelf: 'center'}}>{this.state.errors.username}</Text>}
						    </View>
						    <View style={{width: '100%'}}>
							    <TextInput
							        style={{height: 40, borderColor: 'gray', borderBottomWidth: 1,width: '100%',textAlign: 'center',borderColor: '#fff',marginBottom: 40,color: '#fff',fontSize: 16}}
							        onChangeText={this.onChangeText.bind(this, 'password')}
							        value={password}
							        placeholder={'Password'}
							        placeholderTextColor="#fff"
							        secureTextEntry={true}
							    />
							    {this.state.errors.password && <Text style={{position: 'absolute',bottom: 15,color: 'red',fontSize: 14,alignSelf: 'center'}}>{this.state.errors.password}</Text>}
						    </View>
						    <Button rounded block style={{backgroundColor: '#fff',height: 40,marginBottom: 20}} onPress={this.onSubmit.bind(this)}>
						    	<Text style={{color: 'green'}}>LOGIN</Text>
						    </Button>
					    </View>
			        </ScrollView>
			        </LinearGradient>
			        {<Toast
						ref="toast"
						style={{ backgroundColor: 'rgba(255,0,0,0.4)', width: width - 40 }}
						position='bottom'
						fadeInDuration={1000}
						fadeOutDuration={1000}
						opacity={0.8}
						textStyle={{ color: '#fff',textAlign: 'center' }}
					/>}
		        </Content>
		    </Container>
		)
	}
}
export default connect(state => ({}, mapDispatch))(Login);


const mapDispatch = (dispatch) => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
}