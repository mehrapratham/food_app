import React from 'react'
import { View, Text } from 'react-native'
import { categoryList } from '../../actions/MarketPlace'
import { connect } from 'react-redux'
import {Actions} from 'react-native-router-flux'
class Splash extends React.Component{
	componentDidMount() {
	  setTimeout(() =>{
	  	Actions.Login();
	  }, 1000); 
	}
	render(){
		return(
			<View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
				<Text>Splash</Text>
			</View>
		)
	}
}
export default connect(state => ({}, mapDispatch))(Splash);

const mapDispatch = (dispatch) => {
   const allActionProps = Object.assign({}, dispatch);
   return allActionProps;
}