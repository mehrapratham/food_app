import React from 'react'
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, View,Drawer } from 'native-base';
import {TextInput,Image,Dimensions,ScrollView,TouchableOpacity} from 'react-native'
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import MainHeader from '../../components/common/MainHeader'
import Dialog, { SlideAnimation, DialogContent } from 'react-native-popup-dialog';
import {updateInprogress} from '../../actions/MarketPlace'
 class StartScreen extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			modalVisible: false,
			modalAddTime: false,
			selected: ''
		}
	}

	openModal(){
		this.setState({modalVisible: true})
	}

	confirmOrder(){
		this.setState({modalVisible: false, modalAddTime: true, selected: ''})
	}

	rejectOrder(){
		this.setState({modalVisible: false})
	}

	onPressAddTime(time){
		this.setState({selected: time})
	}

	addTimeAndConfirm(){
		if (this.state.selected != '') {
			let {new_order, inprogress_order} = this.props.Login
			inprogress_order.push(new_order)
			this.props.dispatch(updateInprogress(inprogress_order))
			this.setState({modalAddTime: false})
			Actions.InProgress();
		}
	}

    renderChild(){
    	const slideAnimation = new SlideAnimation({
            slideFrom: 'bottom',
        });
    	let {new_order} = this.props.Login
    	return 	<View style={{alignItems: 'center', padding: 20}}>
    						<View style={{backgroundColor: 'green', width: 80, height: 80, borderRadius: 40, alignItems: 'center', justifyContent: 'center', marginBottom: 20}}>
    							<Text style={{color: '#fff', fontSize: 30}}>1</Text>
    						</View>
    						<View><Text style={{fontSize: 18, marginBottom: 20}}>New Order</Text></View>
    						<Button success style={{alignSelf: 'center'}} onPress={this.openModal.bind(this)}>
			            <Text>Accept Order</Text>
			          </Button>



			          <Dialog
		                    visible={this.state.modalVisible}
		                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
		                    dialogAnimation={slideAnimation}
		                    dialogStyle={{borderRadius: 4,width: '100%',maxHeight: '90%'}}
		                    containerStyle={{justifyContent: 'center',padding: 10}}
		                    onTouchOutside={() => {
		                        this.setState({ modalVisible: false });
		                      }}
		                >
		                    <DialogContent>
		                        <View style={{position: 'relative',paddingTop: 20}}>
		                            <TouchableOpacity
		                                style={{position: 'absolute',right: 0,top:5,paddingLeft: 10,zIndex: 100}}
		                                onPress={() => {
		                                    this.setState({ modalVisible: false });
		                                }}>
		                                <Text style={{ fontSize: 16 }}>X</Text>
		                            </TouchableOpacity>
		                            <ScrollView showsVerticalScrollIndicator={false}>
			                            <View style={{paddingBottom: 10,paddingTop: 20}}>
			                            	<View>
			                            		<Text style={{fontSize: 16,fontWeight: '600', marginBottom:0}}>{new_order.first_name + ' ' +new_order.last_name}</Text>
			                            		<Text style={{position: 'absolute', right: 0, color: 'gray'}}>{new_order.date}</Text>
			                            		<Text style={{marginBottom: 20}}>{new_order.phone}</Text>
			                            		<Text style={{marginBottom: 3}}>Address:</Text>
			                            		<Text>{new_order.address && new_order.address.address1}</Text>
			                            		<Text>{new_order.address && new_order.address.address2}</Text>
			                            		<Text>Pin: {new_order.address && new_order.address.city + ', ' +new_order.address.Pin}</Text>
			                            		
			                            		<Text style={{fontSize: 16,fontWeight: '600', marginBottom:10, marginTop: 20}}>Meals:</Text>
			                            	</View>
				                            {new_order.order_detail && new_order.order_detail.length && new_order.order_detail.map((order_item,key) => {
				                            	return(
				                            		<View style={{borderBottomWidth: 1,borderColor: '#D6D4D4',padding: 10,paddingLeft: 20}} key={key}>
						                            	<View style={{position: 'absolute',left: 0,top: 10}}>
						                            		<Text>{key+1}.</Text>
						                            	</View>
														    						<View style={{marginBottom: 2}}>
														    							<Text style={{fontSize: 14,fontWeight: '600'}}>{order_item.meal_name}</Text>
														    							<View style={{position: 'absolute',right: 0,top: 2}}>
														    								<Text style={{color: '#939090',fontSize: 14}}>{order_item.rate}</Text>
														    							</View>
														    						</View>
														    						<View style={{flexDirection: 'row'}}>
														    							<Text style={{color: '#939090',fontSize: 12}}>{order_item.label}</Text>
														    						</View>
														    					</View>
				                            	)
				                            })}
				                            <View style={{marginTop: 20}}>
				                            	<Text style={{alignSelf: 'flex-end'}}>Total: {new_order.total}</Text>
				                            	<Text style={{alignSelf: 'flex-end', color: 'gray'}}>Delivery Fee: 10.00</Text>
				                            </View>
				                            <View style={{marginTop: 20, flexDirection: 'row'}}>
				                            	<View style={{flex:1, paddingRight: 10}}>
				                            		<Button block dark style={{}} onPress={this.confirmOrder.bind(this)}>
															            <Text>Confirm</Text>
															          </Button>
				                            	</View>
				                            	<View style={{flex:1, paddingLeft:10}}>
				                            		<Button block danger style={{}} onPress={this.rejectOrder.bind(this)}>
															            <Text>Reject</Text>
															          </Button>
				                            	</View>
				                            	
														          
				                            </View>
				                        </View>
			                        </ScrollView>
		                        </View>
		                        
		                    </DialogContent>
		                </Dialog>

		                <Dialog
		                    visible={this.state.modalAddTime}
		                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
		                    dialogAnimation={slideAnimation}
		                    dialogStyle={{borderRadius: 4,width: '100%'}}
		                    containerStyle={{justifyContent: 'center',padding: 10}}
		                    onTouchOutside={() => {
		                        this.setState({ modalAddTime: false });
		                     }}
		                >
		                    <DialogContent>
		                        <View style={{position: 'relative'}}>
		                            <TouchableOpacity
		                                style={{position: 'absolute',right: 0,top:5,paddingLeft: 10,zIndex: 100}}
		                                onPress={() => {
		                                    this.setState({ modalAddTime: false });
		                                }}>
		                                <Text style={{ fontSize: 16 }}>X</Text>
		                            </TouchableOpacity>
		                            <View style={{paddingTop: 20,paddingBottom: 0}}>
		                            	<Text style={{fontSize: 20,marginBottom: 10}}>Delay Order</Text>
		                            	<Text style={{marginBottom: 10}}>How much additional time do you need?we'll notify the customer about the delay</Text>
		                            	<View style={{flexWrap: 'wrap',flexDirection: 'row'}}>
		                            		<Button small style={{borderRadius: 0,borderColor: 'grey',borderWidth: 0.5,backgroundColor: this.state.selected == '10min' ?'#3e9920' : '#fff',marginRight: 10,marginBottom: 10}} onPress={this.onPressAddTime.bind(this,'10min')}>
														            <Text style={{color: this.state.selected == '10min' ?'#fff' : '#000'}}>10 min</Text>
														        </Button>
														        <Button small style={{borderRadius: 0,borderColor: 'grey',borderWidth: 0.5,backgroundColor: this.state.selected == '15min' ?'#3e9920' : '#fff',marginRight: 10,marginBottom: 10}} onPress={this.onPressAddTime.bind(this,'15min')}>
														            <Text style={{color: this.state.selected == '15min' ?'#fff' : '#000'}}>15 min</Text>
														        </Button>
														        <Button small style={{borderRadius: 0,borderColor: 'grey',borderWidth: 0.5,backgroundColor: this.state.selected == '20min' ?'#3e9920' : '#fff',marginRight: 10,marginBottom: 10}} onPress={this.onPressAddTime.bind(this, '20min')}>
														            <Text style={{color: this.state.selected == '20min' ?'#fff' : '#000'}}>20 min</Text>
														        </Button>
														        <Button small style={{borderRadius: 0,borderColor: 'grey',borderWidth: 0.5,backgroundColor: this.state.selected == '25min' ?'#3e9920' : '#fff',marginRight: 10,marginBottom: 10}} onPress={this.onPressAddTime.bind(this, '25min')}>
														            <Text style={{color: this.state.selected == '25min' ?'#fff' : '#000'}}>25 min</Text>
														        </Button>
														        <Button small style={{borderRadius: 0,borderColor: 'grey',borderWidth: 0.5,backgroundColor: this.state.selected == '30min' ?'#3e9920' : '#fff',marginRight: 10,marginBottom: 10}} onPress={this.onPressAddTime.bind(this, '30min')}>
														            <Text style={{color: this.state.selected == '30min' ?'#fff' : '#000'}}>30 min</Text>
														        </Button>
		                            	</View>
		                            	<View style={{alignSelf: 'flex-end',flexDirection: 'row'}}>
		                            		<Button small style={{borderRadius: 0,backgroundColor: '#fff'}} onPress={() => this.setState({ modalAddTime: false })}>
														            <Text style={{color: '#000'}}>CANCEL</Text>
														        </Button>
														        <Button small style={{borderRadius: 0,borderColor: 'grey',borderWidth: 0.5,backgroundColor: !this.state.selected ? 'grey': '#3e9920'}} disabled={!this.state.selected} onPress={this.addTimeAndConfirm.bind(this)}>
														            <Text style={{color: '#fff'}}>NEXT</Text>
														        </Button>
		                            	</View>

		                            </View>
		                        </View>
		                        
		                    </DialogContent>
		                </Dialog>

    					</View>
    }
	
	render(){
		var {height, width} = Dimensions.get('window');
		return(
			<MainHeader child={this.renderChild()} />
		)
	}
}
export default connect(state => ({}, mapDispatch))(StartScreen);


const mapDispatch = (dispatch) => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
}