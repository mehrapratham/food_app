import React from 'react'
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, View,Drawer } from 'native-base';
import {TextInput,Image,Dimensions,ScrollView,TouchableOpacity} from 'react-native'
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import MainHeader from '../../components/common/MainHeader'
import OrderList from '../../components/common/OrderList'
import Dialog, { SlideAnimation, DialogContent } from 'react-native-popup-dialog';
import {selectOrder} from '../../actions/MarketPlace'
 class OrderHistory extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			modalVisible: false,
		}
	}
	openModal(item){
		this.props.dispatch(selectOrder(item))
		this.setState({modalVisible: true})
	}

	renderChild(){
		const slideAnimation = new SlideAnimation({
            slideFrom: 'bottom',
        });
		let {current_order} = this.props.Login
		return <View style={{flex: 1}}>
		        		<ScrollView showsVerticalScrollIndicator={false}>
	    					{this.props.Login.all_orders.length && this.props.Login.all_orders.map((item,key) => {
	    						return(
	    							<OrderList key={key} data={item} activeOpacity={0.6} openModal={this.openModal.bind(this, item)} />
	    						)
	    					})}
    					</ScrollView>
    					<Dialog
		                    visible={this.state.modalVisible}
		                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
		                    dialogAnimation={slideAnimation}
		                    dialogStyle={{borderRadius: 4,width: '100%',maxHeight: '90%'}}
		                    containerStyle={{justifyContent: 'center',padding: 10}}
		                    onTouchOutside={() => {
		                        this.setState({ modalVisible: false });
		                      }}
		                >
		                    <DialogContent>
		                        <View style={{position: 'relative',paddingTop: 20}}>
		                            <TouchableOpacity
		                                style={{position: 'absolute',right: 0,top:5,paddingLeft: 10,zIndex: 100}}
		                                onPress={() => {
		                                    this.setState({ modalVisible: false });
		                                }}>
		                                <Text style={{ fontSize: 16 }}>X</Text>
		                            </TouchableOpacity>
		                            <ScrollView showsVerticalScrollIndicator={false}>
			                            <View style={{paddingBottom: 10,paddingTop: 20}}>
			                            	<View>
			                            		<Text style={{fontSize: 16,fontWeight: '600', marginBottom:0}}>{current_order.first_name + ' ' +current_order.last_name}</Text>
			                            		<Text style={{position: 'absolute', right: 0, color: 'gray'}}>{current_order.date}</Text>
			                            		<Text style={{marginBottom: 20}}>{current_order.phone}</Text>
			                            		<Text style={{marginBottom: 3}}>Address:</Text>
			                            		<Text>{current_order.address && current_order.address.address1}</Text>
			                            		<Text>{current_order.address && current_order.address.address2}</Text>
			                            		<Text>Pin: {current_order.address && current_order.address.city + ', ' +current_order.address.Pin}</Text>
			                            		<View style={{marginTop: 30, marginBottom: 10}}>
			                            			<Text>Status:</Text> 
			                            			<View style={{backgroundColor: current_order.status == 'cancelled' ? 'red' : '#3e9920', paddingLeft: 10, paddingRight:10, borderRadius:10, paddingTop:2, paddingBottom:5, position: 'absolute', left: 60, top: -3}}>
			                            				<Text style={{color: '#fff'}}>{current_order.status}</Text>
			                            			</View>
			                            		</View>
			                            		<Text style={{fontSize: 16,fontWeight: '600', marginBottom:10, marginTop: 20}}>Meals:</Text>
			                            	</View>
				                            {current_order.order_detail && current_order.order_detail.length && current_order.order_detail.map((order_item,key) => {
				                            	return(
				                            		<View style={{borderBottomWidth: 1,borderColor: '#D6D4D4',padding: 10,paddingLeft: 20}} key={key}>
						                            	<View style={{position: 'absolute',left: 0,top: 10}}>
						                            		<Text>{key+1}.</Text>
						                            	</View>
														    						<View style={{marginBottom: 2}}>
														    							<Text style={{fontSize: 14,fontWeight: '600'}}>{order_item.meal_name}</Text>
														    							<View style={{position: 'absolute',right: 0,top: 2}}>
														    								<Text style={{color: '#939090',fontSize: 14}}>{order_item.rate}</Text>
														    							</View>
														    						</View>
														    						<View style={{flexDirection: 'row'}}>
														    							<Text style={{color: '#939090',fontSize: 12}}>{order_item.label}</Text>
														    							{/*<View style={{position: 'absolute',right: 0}}>
														    								<Text style={{color: '#939090',fontSize: 14}}>12/09/2018</Text>
														    							</View>*/}
														    						</View>
														    					</View>
				                            	)
				                            })}
				                            {current_order.status != 'cancelled' && <View>
	  			                            	<Text style={{fontSize: 16,fontWeight: '600', marginBottom:10, marginTop: 20}}>Courier:</Text>
	  			                            	<View style={{paddingLeft: 50, position:'relative'}}>
	  									    								<View style={{width: 40, height: 40, borderRadius: 20, backgroundColor: '#fff', position: 'absolute', left: 0}}>
	  									    									<Image source={require('../../img/user.png')} style={{width: '100%',height: '100%'}}/>
	  									    								</View>
	  									    								<Text style={{}}>{current_order.deliver_by && current_order.deliver_by.name}</Text>
	  									    								<Text style={{}}>{current_order.deliver_by && current_order.deliver_by.phone}</Text>
	  									    							</View>
	  			                            </View>}
				                            <View style={{marginTop: 20}}>
				                            	<Text style={{alignSelf: 'flex-end'}}>Total: {current_order.total}</Text>
				                            	<Text style={{alignSelf: 'flex-end', color: 'gray'}}>Delivery Fee: 10.00</Text>
				                            </View>
				                        </View>
			                        </ScrollView>
		                        </View>
		                        
		                    </DialogContent>
		                </Dialog>
		        	</View>
	}
	
	render(){
		var {height, width} = Dimensions.get('window');
		
		return(
			<MainHeader child={this.renderChild()} />
		)
	}
}
export default connect(state => ({}, mapDispatch))(OrderHistory);


const mapDispatch = (dispatch) => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
}