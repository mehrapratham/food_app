import axios from 'axios'
import { CATEGORY_URL } from '../apiConstant'
import { HAS_ERROR, CURRENT_ORDER, UPDATE_INPROGRESS, UPDATE_ORDER_HISTORY } from '../types'
import {AsyncStorage} from 'react-native'
import store from '../../store'

export function selectOrder(item) {
    return {
        type: CURRENT_ORDER,
        payload: item,
    };
}
export function updateInprogress(data) {
    return {
        type: UPDATE_INPROGRESS,
        payload: data,
    };
}
export function updateOrderHistory(data) {
    return {
        type: UPDATE_ORDER_HISTORY,
        payload: data,
    };
}

