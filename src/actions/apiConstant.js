const dev_server = '';

/**
* Base URL for all the APIs
*/
const BaseUrl = dev_server;

/**
* API Constanst for different actions which are used to call an particular API
*/
// export const BASE_URL                =           BaseUrl+':3000/'
export const CATEGORY_URL            =           BaseUrl+'/category'

