import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import Splash from './containers/Splash';
import StartScreen from './containers/StartScreen';
import Login from './containers/Login';
import OrderHistory from './containers/OrderHistory';
import Inprogress from './containers/Inprogress';
export default class App extends Component {
  render() {
    console.disableYellowBox = true;
    return (
      <Router hideNavBar= "true">
        <Scene key="root" duration={0}>
          <Scene key="Splash" component={Splash} hideNavBar title="PageOne" />
          <Scene key="StartScreen" component={StartScreen} hideNavBar title="PageTwo" />
          <Scene key="Login" component={Login} hideNavBar title="Login" initial={true} />
          <Scene key="OrderHistory" component={OrderHistory} hideNavBar title="OrderHistory"/>
          <Scene key="InProgress" component={Inprogress} hideNavBar title="Inprogress"  />
        </Scene>
      </Router>
    )
  }
}